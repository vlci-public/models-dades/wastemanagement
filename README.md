# wasteManagement

Models de dades del projecte VLCi basats en els data models Fiware: https://github.com/smart-data-models/dataModel.WasteManagement

### List of data models

The following entity types are available:
- [WasteContainer](https://gitlab.com/vlci-public/models-dades/wastemanagement/-/blob/main/WasteContainer/README.md). A waste container.