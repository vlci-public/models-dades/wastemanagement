**Índice**  

 [[_TOC_]]  
  

# Entidad: WasteContainer

[Basado en la entidad Fiware WasteContainer](https://github.com/smart-data-models/dataModel.WasteManagement)

Descripción global: **Una observación de los parámetros de medicición de los  contenedores de residuos.**

Utiliza los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md).

## Lista de propiedades

##### Atributos de la entidad context broker

- `id[string]`: Identificador único de la entidad
- `type[string]`: NGSI Tipo de entidad

##### Atributos descriptivos de la entidad

- `location[geo:json]`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon
- `address[string]`: Dirección civil donde el contenedor está ubicado.
- `project[string]`: Proyecto al que pertenece esta entidad.
- `refDevice`: Referencia a la entidad Device del dispositivo usado para monitorizar el contendor.

##### Atributos descriptivos de la entidad (OPCIONALES)
- `serialNumber[string]`: Número de serie del contador.
- `category[list]`: Categoría(s) del contenedor.
- `storedWasteKind[string]`: Tipo de residuo almacenado.
- `binCapacity[string]`: Capacidad de almacenaje.
- `description[string]`: Descripción del contenedor.
- `depth[number]`: Profundidad del contenedor. Se mide en metros.
- `height[number]`: Altura del contenedor. Se mide en metros.
- `madeOf[string]`: Material del que está hecho el contendor.
- `cargoVolume[number]`: Volumen total que el contenedor puede soportar. Se mide en Litros.
- `modelName[string]`: Nombre del modelo otorgado por el fabricante del contenedor.
- `offset_height[number]`: Offset de altura: distancia entre la ubicación del sensor y la entrada de los residuos (lo que se indica como distancia útil). Se mide en cm.

##### Atributos de la medición

- `TimeInstant[datetime]` : Marca de tiempo salvada por el IoT Agent cuando el dato dinámico se recibe.
- `fillingLevel[number]`: Porcentaje de llenado del contenedor en partes por 1.
- `temeprature[number]`: Temperatura dentro del contenedor. Se mide en ºC.
- `c_factor[number]`: Factor C: factor multiplicativo entre [0 - 2] para adecuar la medida tomada por el sensor con el % visual del llenado que el cliente tiene del mismo.

##### Atributos para la monitorización

- `operationalStatus[string]`: Posibles valores: ok, noData.
- `maintenanceOwner[string]`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail[string]`: Email del responsable técnico de esta entidad.
- `serviceOwner[string]`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail[string]`: Email de la persona del servicio municipal de contacto para esta entidad.
- `inactive[bool]`: (Boolean) Posibles valores: true, false. True - la entidad está de baja o inactiva. Se usa para no tenerla en cuenta de cara a la monitorización de su estado. Está pensado para casos en los que la entidad se sabe a priori que no se va a actualizar nunca y por tanto se quiere que los procesos que hacen uso de ese operationalStatus ignoren esta propiedad.

## Lista de entidades que implementan este modelo (531 entidades)

| Subservicio | ID Entidad                   |
| ----------- | ---------------------------- |
| /residuos |  wastecontainer:XXXX  |
| /residuos |  aceiteContenedoresDipu2023XXXX|

Entidades provenientes de 3 implementaciones diferentes:
- Proyecto Impulso. 276 entidades. Id entidad: 
- Diputación de Valencia. 106 entidades. Id entidad: 
- Integrador: Sayme. 164 entidades. Id entidad: 


##### Ejemplo de entidad

```
curl --location --request GET 'https://cb.vlci.valencia.es:10027/v2/entities/wastecontainer:5ddbfc546c54998b7f89fab1' \
--header 'Fiware-Service: sc_vlci' \
--header 'Fiware-ServicePath: /residuos' \
--header 'X-Auth-Token: INSERT_YOUR_TOKEN_HERE'
```

##### Ejemplo de envio de datos mediante suscripcion desde la plataforma Context Broker (NGSI v2)

<details><summary>Click para ver la respuesta</summary>
{
  "method": "POST",
  "path": "/",
  "query": {},
  "client_ip": "XXXXX",
  "url": "XXXXX",
  "headers": {
    "host": "XXXXX",
    "content-length": "1561",
    "user-agent": "orion/4.0.0 libcurl/7.88.1",
    "fiware-service": "sc_vlci",
    "fiware-servicepath": "/residuos",
    "accept": "application/json",
    "content-type": "application/json; charset=utf-8",
    "fiware-correlator": "24ae3d44-38d4-11ed-9aad-0a580a810238; cbnotif=2; node=2I_aNjd2H1; perseocep=2071940; cbnotif=3",
    "ngsiv2-attrsformat": "normalized"
  },
  {
  "subscriptionId": "XXXXX",
  "data": [
    {
      "id": "wastecontainer:5ddbfc546c54998b7f89fab1",
      "type": "WasteContainer",
      "TimeInstant": {
        "type": "DateTime",
        "value": "2022-01-14T06:01:53.167Z",
        "metadata": {}
      },
      "c_factor": {
        "type": "Number",
        "value": 1,
        "metadata": {}
      },
      "cargoVolume": {
        "type": "Number",
        "value": 0,
        "metadata": {}
      },
      "category": {
        "type": "StructuredValue",
        "value": [
          "Superficie"
        ],
        "metadata": {}
      },
      "depth": {
        "type": "Number",
        "value": 0.66,
        "metadata": {}
      },
      "fillingLevel": {
        "type": "Number",
        "value": 0,
        "metadata": {
          "TimeInstant": {
            "type": "DateTime",
            "value": "2022-01-14T06:01:53.167Z"
          }
        }
      },
      "height": {
        "type": "Number",
        "value": 1.08,
        "metadata": {}
      },
      "location": {
        "type": "geo:json",
        "value": {
          "type": "Point",
          "coordinates": [
            -0.3779886,
            39.485488396
          ]
        },
        "metadata": {}
      },
      "madeOf": {
        "type": "Text",
        "value": "other",
        "metadata": {}
      },
      "maintenanceOwner": {
        "type": "Text",
        "value": "Wellness ",
        "metadata": {}
      },
      "maintenanceOwnerEmail": {
        "type": "Text",
        "value": "supportsmart@wellnesstg.com",
        "metadata": {}
      },
      "modelName": {
        "type": "Text",
        "value": "-",
        "metadata": {}
      },
      "offset_height": {
        "type": "Number",
        "value": 42,
        "metadata": {}
      },
      "operationalStatus": {
        "type": "Text",
        "value": "noData",
        "metadata": {}
      },
      "refDevice": {
        "type": "Relationship",
        "value": "device:wt:866039045930030",
        "metadata": {}
      },
      "serialNumber": {
        "type": "Text",
        "value": "-",
        "metadata": {}
      },
      "serviceOwner": {
        "type": "Text",
        "value": "Antonio Molla Calabuig",
        "metadata": {}
      },
      "serviceOwnerEmail": {
        "type": "Text",
        "value": "resisolidos@valencia.es",
        "metadata": {}
      },
      "temperature": {
        "type": "Number",
        "value": 6,
        "metadata": {
          "TimeInstant": {
            "type": "DateTime",
            "value": "2022-01-14T06:01:53.167Z"
          }
        }
      }
    }
  ]
}

</details>

# Atributos que van al GIS para su representación en una capa

No envía datos al GIS.