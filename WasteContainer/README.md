Especificación más reciente
===
Enlace a la última/más reciente [Especificación](https://gitlab.com/vlci-public/models-dades/wastemanagement/-/blob/main/WasteContainer/spec.md) que se tiene . Contiene entidades de 3 proyectos diferentes: 
- Contenedores del proyecto Impulso. 
- Contenedores de la Diputación. 
- Contenedores de aceite.